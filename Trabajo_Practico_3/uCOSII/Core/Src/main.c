/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include "DIO.h"
#include "stdio.h"
#include <math.h>
#include "lib_str.h"
#include "stdarg.h"
#include "usbd_cdc_if.h"
#include <time.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 16u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static void StartupTask (void *p_arg);
static void DAC_Task (void *p_arg);
static void Frequency_Task (void *p_arg);
static void Sec1_Task (void *p_arg);
static void Sec2_Task (void *p_arg);
static void Sec3_Task (void *p_arg);
static void Input_Scan (void *p_arg);
static void Output_Screen (void *p_arg);
static void Led_Task (void *p_arg);

static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK DACTaskStk[APP_CFG_DAC_TASK_STK_SIZE];
static OS_STK FreqTaskStk[APP_CFG_Freq_TASK_STK_SIZE];
static OS_STK Sec1TaskStk[APP_CFG_Sec1_TASK_STK_SIZE];
static OS_STK Sec2TaskStk[APP_CFG_Sec2_TASK_STK_SIZE];
static OS_STK Sec3TaskStk[APP_CFG_Sec3_TASK_STK_SIZE];
static OS_STK InputTaskStk[APP_CFG_Input_TASK_STK_SIZE];
static OS_STK ScreenTaskStk[APP_CFG_Screen_TASK_STK_SIZE];
static OS_STK LedTaskStk[APP_CFG_LED_TASK_STK_SIZE];

static void App_TaskCreate (void);

static void App_EventCreate (void);
OS_EVENT *modeMbox;
OS_EVENT *periodMbox;
OS_EVENT *sequenceMbox;
OS_EVENT *freq_ammountMbox;

void UsbPrintf (CPU_CHAR  *p_fmt, ...);

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
  {
  /* Set all external intr. to KA interrupt priority boundary */
  CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
  }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  OSInit();
  DIOInit();
  OSTaskCreateExt( StartupTask,
                   0,
				   &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
				   APP_CFG_STARTUP_TASK_PRIO,
				   APP_CFG_STARTUP_TASK_PRIO,
				   &StartupTaskStk[0],
				   APP_CFG_STARTUP_TASK_STK_SIZE,
				   0,
				   (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
  #if (OS_TASK_NAME_EN > 0u)
   OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
    	  	      (INT8U *)"Startup task",
				  &os_err);
  #endif
  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  //Se movio a DIO
}

/* USER CODE BEGIN 4 */
#warning "Se mueven los contenidos de MX_GPIO_Init a DIOInit"
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskCreate()
*
* Description : This task creates other tasks.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void App_EventCreate(void)
{
	modeMbox = OSMboxCreate((void *)0);
	periodMbox = OSMboxCreate((void *)0);
	sequenceMbox = OSMboxCreate((void *)0);
	freq_ammountMbox = OSMboxCreate((void *)0);
}

static void App_TaskCreate (void)
{
	CPU_INT08U os_err;

	os_err = OSTaskCreateExt((void (*)(void *)) Led_Task,
                            (void           * ) 0,
							(OS_STK         * )&LedTaskStk[APP_CFG_LED_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_LED_TASK_PRIO,
							(INT16U           ) APP_CFG_LED_TASK_PRIO,
							(OS_STK         * )&LedTaskStk[0],
							(INT32U           ) APP_CFG_LED_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_LED_TASK_PRIO, (INT8U *) "Led Task", &os_err);


	os_err = OSTaskCreateExt((void (*)(void *)) DAC_Task,
							(void           * ) 0,
							(OS_STK         * )&DACTaskStk[APP_CFG_DAC_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_DAC_TASK_PRIO,
							(INT16U           ) APP_CFG_DAC_TASK_PRIO,
							(OS_STK         * )&DACTaskStk[0],
							(INT32U           ) APP_CFG_DAC_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_DAC_TASK_PRIO, (INT8U *) "DAC Task", &os_err);
	OSTaskSuspend(APP_CFG_DAC_TASK_PRIO);


	os_err = OSTaskCreateExt((void (*)(void *)) Frequency_Task,
							(void           * ) 0,
							(OS_STK         * )&FreqTaskStk[APP_CFG_Freq_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Freq_TASK_PRIO,
							(INT16U           ) APP_CFG_Freq_TASK_PRIO,
							(OS_STK         * )&FreqTaskStk[0],
							(INT32U           ) APP_CFG_Freq_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Freq_TASK_PRIO, (INT8U *) "Frequency meter Task", &os_err);


	os_err = OSTaskCreateExt((void (*)(void *)) Input_Scan,
							(void           * ) 0,
							(OS_STK         * )&InputTaskStk[APP_CFG_Input_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Input_TASK_PRIO,
							(INT16U           ) APP_CFG_Input_TASK_PRIO,
							(OS_STK         * )&InputTaskStk[0],
							(INT32U           ) APP_CFG_Input_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Input_TASK_PRIO, (INT8U *) "Input Task", &os_err);


	os_err = OSTaskCreateExt((void (*)(void *)) Sec1_Task,
							(void           * ) 0,
							(OS_STK         * )&Sec1TaskStk[APP_CFG_Sec1_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Sec1_TASK_PRIO,
							(INT16U           ) APP_CFG_Sec1_TASK_PRIO,
							(OS_STK         * )&Sec1TaskStk[0],
							(INT32U           ) APP_CFG_Sec1_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Sec1_TASK_PRIO, (INT8U *) "Sequence 1 Task", &os_err);
	OSTaskSuspend(APP_CFG_Sec1_TASK_PRIO);


	os_err = OSTaskCreateExt((void (*)(void *)) Sec2_Task,
							(void           * ) 0,
							(OS_STK         * )&Sec2TaskStk[APP_CFG_Sec2_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Sec2_TASK_PRIO,
							(INT16U           ) APP_CFG_Sec2_TASK_PRIO,
							(OS_STK         * )&Sec2TaskStk[0],
							(INT32U           ) APP_CFG_Sec2_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Sec2_TASK_PRIO, (INT8U *) "Sequence 2 Task", &os_err);
	OSTaskSuspend(APP_CFG_Sec2_TASK_PRIO);


	os_err = OSTaskCreateExt((void (*)(void *)) Sec3_Task,
							(void           * ) 0,
							(OS_STK         * )&Sec3TaskStk[APP_CFG_Sec3_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Sec3_TASK_PRIO,
							(INT16U           ) APP_CFG_Sec3_TASK_PRIO,
							(OS_STK         * )&Sec3TaskStk[0],
							(INT32U           ) APP_CFG_Sec3_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Sec3_TASK_PRIO, (INT8U *) "Sequence 3 Task", &os_err);
	OSTaskSuspend(APP_CFG_Sec3_TASK_PRIO);


	os_err = OSTaskCreateExt((void (*)(void *)) Output_Screen,
							(void           * ) 0,
							(OS_STK         * )&ScreenTaskStk[APP_CFG_Screen_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_Screen_TASK_PRIO,
							(INT16U           ) APP_CFG_Screen_TASK_PRIO,
							(OS_STK         * )&ScreenTaskStk[0],
							(INT32U           ) APP_CFG_Screen_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_Screen_TASK_PRIO, (INT8U *) "Screen output Task", &os_err);



}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void StartupTask (void *p_arg)
{
	CPU_INT32U cpu_clk;
	(void)p_arg;
	cpu_clk = HAL_RCC_GetHCLKFreq();
	/* Initialize and enable System Tick timer */
	OS_CPU_SysTickInitFreq(cpu_clk);

	#if (OS_TASK_STAT_EN > 0)
	OSStatInit();                                               /* Determine CPU capacity.                              */
	#endif

	// App_EventCreate();                                          /* Create application events.                           */
	App_TaskCreate();                                           /* Create application tasks.                            */

	App_EventCreate(); //Crear eventos.

	OSTaskDel(3u);
}





/*
*********************************************************************************************************
*                                          Led_Task()
*
* Description : Tarea que hace parpadear el led cada 500 ms
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Led_Task (void *p_arg)
{
	while (DEF_TRUE){
		HAL_GPIO_TogglePin(Led_GPIO_Port, Led_Pin);
		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}


/*
*********************************************************************************************************
*                                          DAC_Task()
*
* Description : Tarea que genera la salida sinoidal y la manda al DAC
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

int frequency = 50;
static void DAC_Task (void *p_arg)
{
	int SINE_OFFSET = 127;
	int SINE_AMPLITUDE = 127;

	int sine,j;
	int count = 0;

	while (DEF_TRUE){
//		task_delay = (INT16U) (1000/(frequency));

		count+=frequency;
		if(count>1000){
			count=0;
		}

		//Consigo el valor del sinusoide con centro en 127 y amplitud 127
		sine = (int) (SINE_OFFSET + SINE_AMPLITUDE * sin(count * 2 * M_PI / 1000));

		//Convierto el valor (0-254) de decimal a binario
		for(j=0;j<8;j++){
			DOSet(j+3, sine%2);
			sine=sine/2;
		}

//		OSTimeDlyHMSM(0u, 0u, 0u, task_delay);
		OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	}
}



/*
*********************************************************************************************************
*                                          Frequency_Task()
*
* Description : Tarea lee la frecuencia de un pin
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

int reset_period=0;
static void Frequency_Task (void *p_arg)
{
	int last_value=0;
	int flag=1;
	double period=0;
	uint32_t begin_tick;
	uint32_t end_tick;

	int TICK_RATE = 1; //1 ms de tick
	while (DEF_TRUE){
		if (DIGet(4)){
			if (!last_value){//Leo el flanco ascendente
				if(flag){
					begin_tick = HAL_GetTick();
					flag = 0;
				}else{
					end_tick = HAL_GetTick();
					flag = 1;

					if ((end_tick-begin_tick)!=0){
						period = (double) ((end_tick-begin_tick) / TICK_RATE);
					}
				}
				last_value=1;
			}
		}else{
			last_value=0;
		}

		if(reset_period){
			period=0;
			reset_period=0;
		}

		OSMboxPost(periodMbox, (void *)&period);

		OSTimeDlyHMSM(0u, 0u, 0u, 1u);
	}
}

/*
*********************************************************************************************************
*                                          Input_Scan()
*
* Description : Tarea que lee las entradas del sistema
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Input_Scan (void *p_arg)
{
	int mode = 1; //0 = Sintetizada, 1 = Individuales
	int sequence = 0;
	int freq_ammount = 10;
	int j;

	while (DEF_TRUE){
		//Envio el valor de las variables a Output_Screen()
		OSMboxPost(modeMbox, (void *)&mode);
		OSMboxPost(sequenceMbox, (void *)&sequence);
		OSMboxPost(freq_ammountMbox, (void *)&freq_ammount);

		if(DIGet(0)){ //Switch 1
			//Cambio de modo individual a sintetizada con suspend y resume
			if(mode){
				OSTaskSuspend(APP_CFG_Freq_TASK_PRIO);
				switch(sequence){
					case 1:
						OSTaskSuspend(APP_CFG_Sec1_TASK_PRIO);
						break;
					case 2:
						OSTaskSuspend(APP_CFG_Sec2_TASK_PRIO);
						break;
					case 3:
						OSTaskSuspend(APP_CFG_Sec3_TASK_PRIO);
						break;
					default:
						break;
				}
				DOSet(0,0);
				DOSet(1,0);
				DOSet(2,0);
				OSTaskResume(APP_CFG_DAC_TASK_PRIO); //Resumo la task del DAC
				mode = 0;
			}else{
				OSTaskSuspend(APP_CFG_DAC_TASK_PRIO);
				switch(sequence){
					case 1:
						OSTaskResume(APP_CFG_Sec1_TASK_PRIO);
						break;
					case 2:
						OSTaskResume(APP_CFG_Sec2_TASK_PRIO);
						break;
					case 3:
						OSTaskResume(APP_CFG_Sec3_TASK_PRIO);
						break;
					default:
						break;
				}
				for(j=0;j<8;j++){
					DOSet(j+3, 0);
				}
				reset_period = 1;
				OSTaskResume(APP_CFG_Freq_TASK_PRIO); //Resumo la task del frecuencimetro
				mode = 1;
			}
		}else if(DIGet(1)){ //Switch 2
			//Individual: Secuencia 1
			//Sintetizada: Aumentar frec
			if(mode){
				switch(sequence){
					case 1:
						break;
					case 2:
						OSTaskSuspend(APP_CFG_Sec2_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec1_TASK_PRIO);
						break;
					case 3:
						OSTaskSuspend(APP_CFG_Sec3_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec1_TASK_PRIO);
						break;
					default:
						OSTaskResume(APP_CFG_Sec1_TASK_PRIO);
						break;
				}
				DOSet(0,0);
				sequence = 1;
			}else{
				if(frequency+freq_ammount<250){
					frequency += freq_ammount;
				}else{
					frequency = 250;
				}
			}
		}else if(DIGet(2)){ //Switch 3
			//Individual: Secuencia 2
			//Sintetizada: Disminuir frec
			if(mode){
				switch(sequence){
					case 1:
						OSTaskSuspend(APP_CFG_Sec1_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec2_TASK_PRIO);
						break;
					case 2:
						break;
					case 3:
						OSTaskSuspend(APP_CFG_Sec3_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec2_TASK_PRIO);
						break;
					default:
						OSTaskResume(APP_CFG_Sec2_TASK_PRIO);
						break;
				}
				sequence = 2;
			}else{
				if(frequency-freq_ammount>1){
					frequency -= freq_ammount;
				}else{
					frequency = 1;
				}
			}
		}else if(DIGet(3)){ //Switch 4
			//Individual: Secuencia 3
			//Sintetizada: Cambiar cuanto aumentar o disminuir de frecuencia.
			if(mode){
				switch(sequence){
					case 1:
						OSTaskSuspend(APP_CFG_Sec1_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec3_TASK_PRIO);
						break;
					case 2:
						OSTaskSuspend(APP_CFG_Sec2_TASK_PRIO);
						OSTaskResume(APP_CFG_Sec3_TASK_PRIO);
						break;
					case 3:
						break;
					default:
						OSTaskResume(APP_CFG_Sec3_TASK_PRIO);
						break;
				}
				sequence = 3;
			}else{
				switch(freq_ammount){
					case 1:
						freq_ammount=10;
						break;
					case 10:
						freq_ammount=100;
						break;
					case 100:
						freq_ammount=1;
						break;
					default:
						freq_ammount=10;
						break;
				}
			}
		}
		OSTimeDlyHMSM(0u, 0u, 0u, 200u);
	}
}

/*
*********************************************************************************************************
*                                          Sec1_Task()
*                                          Sec2_Task()
*                                          Sec3_Task()
*
* Description : Tareas de la secuencias de luces
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Sec1_Task (void *p_arg)
{
	int s1_patron_1[4] = {1,0,1,0};
	int s1_patron_2[4] = {1,1,0,0};
	int count = 0;

	while (DEF_TRUE){
		//Escribir la tabla de DIO y mandarlo a los pines
		DOSet(2,s1_patron_1[count]);
		DOSet(1,s1_patron_2[count]);

		count++;
		if(count>=4){
			count=0;
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

static void Sec2_Task (void *p_arg)
{
	int s2_patron_1[4] = {1,0,0,0};
	int s2_patron_2[4] = {1,1,0,0};
	int s2_patron_3[4] = {1,1,1,0};
	int count = 0;

	while (DEF_TRUE){
		DOSet(2, s2_patron_1[count]);
		DOSet(1, s2_patron_2[count]);
		DOSet(0, s2_patron_3[count]);

		count++;
		if(count>=4){
			count=0;
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

static void Sec3_Task (void *p_arg)
{
	int s3_patron_1[4] = {1,1,0,0};
	int s3_patron_2[6] = {1,1,1,0,0,0};
	int s3_patron_3[7] = {1,1,1,1,0,0,0};
	int count1 = 0;
	int count2 = 0;
	int count3 = 0;

	while (DEF_TRUE){
		DOSet(2, s3_patron_1[count1]);
		DOSet(1, s3_patron_2[count2]);
		DOSet(0, s3_patron_3[count3]);

		count1++;
		count2++;
		count3++;
		if(count1>=4){
			count1=0;
		}if(count2>=6){
			count2=0;
		}if(count3>=7){
			count3=0;
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 50u);
	}
}

/*
*********************************************************************************************************
*                                          Screen_Task()
*
* Description : Tarea que imprime por pantalla los datos.
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Output_Screen (void *p_arg)
{
	int *screen_mode;
	int *p_seq, *p_dac_mult;
	double *p_pe;
	int fr, int_pe;
	double pe;
	INT8U err;
	while (DEF_TRUE){
		//Recibir datos de las variables por evento
		screen_mode = (int *) OSMboxPend(modeMbox, 10, &err);
		p_pe = (double *) OSMboxPend(periodMbox, 10, &err);
		pe= *p_pe;
		p_seq = (int *) OSMboxPend(sequenceMbox, 10, &err);
		p_dac_mult = (int *) OSMboxPend(freq_ammountMbox, 10, &err);

		if ((*screen_mode & 0b1) == 1){
			if(pe>0){
				fr = (int) 1000/(pe);
				int_pe = (int) pe;
				UsbPrintf("\rIndividual. Secuencia %d. Frecuencia: %d Hz. Periodo: %d ms           ", *p_seq, fr, int_pe);
				reset_period=1;
			}else{
				UsbPrintf("\rIndividual. Secuencia %d. Frecuencia: No encontrada                   ", *p_seq);
			}
		}else{
			UsbPrintf    ("\rLa frecuencia de salida es de %d [Hz]. Multiplicador: x%d             ", frequency, *p_dac_mult);
		}


		OSTimeDlyHMSM(0u, 0u, 1u, 0u);
	}
}

/*
**************************************************************************************************************************
*                                               UsbPrintfMsg()
*
* Description :
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/
void UsbPrintf (CPU_CHAR  *p_fmt, ...)
{
    CPU_CHAR    str[80u + 1u];
    CPU_SIZE_T  len;
    va_list     vArgs;

    va_start(vArgs, p_fmt);

    vsprintf((char       *)str,
             (char const *)p_fmt,
                           vArgs);

    va_end(vArgs);

    len = Str_Len(str);

    CDC_Transmit_FS((uint8_t *)str, len);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
