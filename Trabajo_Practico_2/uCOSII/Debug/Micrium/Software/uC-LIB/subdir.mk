################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Micrium/Software/uC-LIB/lib_ascii.c \
../Micrium/Software/uC-LIB/lib_math.c \
../Micrium/Software/uC-LIB/lib_mem.c \
../Micrium/Software/uC-LIB/lib_str.c 

OBJS += \
./Micrium/Software/uC-LIB/lib_ascii.o \
./Micrium/Software/uC-LIB/lib_math.o \
./Micrium/Software/uC-LIB/lib_mem.o \
./Micrium/Software/uC-LIB/lib_str.o 

C_DEPS += \
./Micrium/Software/uC-LIB/lib_ascii.d \
./Micrium/Software/uC-LIB/lib_math.d \
./Micrium/Software/uC-LIB/lib_mem.d \
./Micrium/Software/uC-LIB/lib_str.d 


# Each subdirectory must supply rules for building sources it contributes
Micrium/Software/uC-LIB/%.o Micrium/Software/uC-LIB/%.su: ../Micrium/Software/uC-LIB/%.c Micrium/Software/uC-LIB/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Micrium/Template/OS2 -I../Micrium/Software/uC-CPU/ARM-Cortex-M/ARMv7-M/GNU -I../Micrium/Template -I../Micrium/Software/uCOS-II/Source -I../Micrium/Software/uC-LIB -I../Micrium/Software/uC-CPU -I../Micrium/Software/uCOS-II/Ports/ARM-Cortex-M/ARMv7-M/GNU -I../USB_DEVICE/Target -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -I../USB_DEVICE/App -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Micrium-2f-Software-2f-uC-2d-LIB

clean-Micrium-2f-Software-2f-uC-2d-LIB:
	-$(RM) ./Micrium/Software/uC-LIB/lib_ascii.d ./Micrium/Software/uC-LIB/lib_ascii.o ./Micrium/Software/uC-LIB/lib_ascii.su ./Micrium/Software/uC-LIB/lib_math.d ./Micrium/Software/uC-LIB/lib_math.o ./Micrium/Software/uC-LIB/lib_math.su ./Micrium/Software/uC-LIB/lib_mem.d ./Micrium/Software/uC-LIB/lib_mem.o ./Micrium/Software/uC-LIB/lib_mem.su ./Micrium/Software/uC-LIB/lib_str.d ./Micrium/Software/uC-LIB/lib_str.o ./Micrium/Software/uC-LIB/lib_str.su

.PHONY: clean-Micrium-2f-Software-2f-uC-2d-LIB

