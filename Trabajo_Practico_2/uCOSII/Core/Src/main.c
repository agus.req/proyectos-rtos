/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include "stdio.h"
#include <math.h>
#include "lib_str.h"
#include "stdarg.h"
#include "usbd_cdc_if.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 16u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static void StartupTask (void *p_arg);
static void Led_Task (void *p_arg);
static void Switches_Task (void *p_arg);
static void Digital_Clock_Task (void *p_arg);
static void Show_Clock_Task (void *p_arg);

static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK LedTaskStk[APP_CFG_LED_TASK_STK_SIZE];
static OS_STK SwitchesTaskStk[APP_CFG_SWITCHES_TASK_STK_SIZE];
static OS_STK DigitalClockTaskStk[APP_CFG_DIGITAL_CLOCK_TASK_STK_SIZE];
static OS_STK ShowClockTaskStk[APP_CFG_SHOW_CLOCK_TASK_STK_SIZE];

static void App_TaskCreate (void);

void UsbPrintf (CPU_CHAR  *p_fmt, ...);

uint8_t switch1;
uint8_t switch2;
uint8_t switch3;
uint8_t switch4;

int start = 0;
int asc_desc = 0; // 0 = Asc , 1 = Desc

int timer_min = 0;
int timer_sec = 0;
int timer_ms = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
  {
  /* Set all external intr. to KA interrupt priority boundary */
  CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
  }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  OSInit();
  OSTaskCreateExt( StartupTask,
                   0,
				   &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
				   APP_CFG_STARTUP_TASK_PRIO,
				   APP_CFG_STARTUP_TASK_PRIO,
				   &StartupTaskStk[0],
				   APP_CFG_STARTUP_TASK_STK_SIZE,
				   0,
				   (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
  #if (OS_TASK_NAME_EN > 0u)
   OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
    	  	      (INT8U *)"Startup task",
				  &os_err);
  #endif
  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(Led_GPIO_Port, Led_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : Led_Pin */
  GPIO_InitStruct.Pin = Led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(Led_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Sw4_Pin Sw3_Pin Sw2_Pin Sw1_Pin */
  GPIO_InitStruct.Pin = Sw4_Pin|Sw3_Pin|Sw2_Pin|Sw1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskCreate()
*
* Description : This task creates other tasks.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void App_TaskCreate (void)
{
	CPU_INT08U os_err;

	os_err = OSTaskCreateExt((void (*)(void *)) Led_Task,
                            (void           * ) 0,
							(OS_STK         * )&LedTaskStk[APP_CFG_LED_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_LED_TASK_PRIO,
							(INT16U           ) APP_CFG_LED_TASK_PRIO,
							(OS_STK         * )&LedTaskStk[0],
							(INT32U           ) APP_CFG_LED_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_LED_TASK_PRIO, (INT8U *) "Led Task", &os_err);



	os_err = OSTaskCreateExt((void (*)(void *)) Switches_Task,
							(void           * ) 0,
							(OS_STK         * )&SwitchesTaskStk[APP_CFG_SWITCHES_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_SWITCHES_TASK_PRIO,
							(INT16U           ) APP_CFG_SWITCHES_TASK_PRIO,
							(OS_STK         * )&SwitchesTaskStk[0],
							(INT32U           ) APP_CFG_SWITCHES_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_SWITCHES_TASK_PRIO, (INT8U *) "Switches Task", &os_err);



	os_err = OSTaskCreateExt((void (*)(void *)) Digital_Clock_Task,
							(void           * ) 0,
							(OS_STK         * )&DigitalClockTaskStk[APP_CFG_DIGITAL_CLOCK_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_DIGITAL_CLOCK_TASK_PRIO,
							(INT16U           ) APP_CFG_DIGITAL_CLOCK_TASK_PRIO,
							(OS_STK         * )&DigitalClockTaskStk[0],
							(INT32U           ) APP_CFG_DIGITAL_CLOCK_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_DIGITAL_CLOCK_TASK_PRIO, (INT8U *) "Digital Clock Task", &os_err);



	os_err = OSTaskCreateExt((void (*)(void *)) Show_Clock_Task,
							(void           * ) 0,
							(OS_STK         * )&ShowClockTaskStk[APP_CFG_SHOW_CLOCK_TASK_STK_SIZE - 1],
							(INT8U            ) APP_CFG_SHOW_CLOCK_TASK_PRIO,
							(INT16U           ) APP_CFG_SHOW_CLOCK_TASK_PRIO,
							(OS_STK         * )&ShowClockTaskStk[0],
							(INT32U           ) APP_CFG_SHOW_CLOCK_TASK_STK_SIZE,
							(void           * ) 0,
							(INT16U           ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_SHOW_CLOCK_TASK_PRIO, (INT8U *) "Show Clock Task", &os_err);
}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void StartupTask (void *p_arg)
{
	CPU_INT32U cpu_clk;
	(void)p_arg;
	cpu_clk = HAL_RCC_GetHCLKFreq();
	/* Initialize and enable System Tick timer */
	OS_CPU_SysTickInitFreq(cpu_clk);

	#if (OS_TASK_STAT_EN > 0)
	OSStatInit();                                               /* Determine CPU capacity.                              */
	#endif

	// App_EventCreate();                                          /* Create application events.                           */
	App_TaskCreate();                                           /* Create application tasks.                            */

	OSTaskDel(3u);
}





/*
*********************************************************************************************************
*                                          Led_Task()
*
* Description : Tarea que hace parpadear el led cada 500 ms
*
* Argument(s) : p_arg       Argument passed to 'Led_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Led_Task (void *p_arg)
{
	while (DEF_TRUE){
		HAL_GPIO_TogglePin(Led_GPIO_Port, Led_Pin);
		if(!start){
			UsbPrintf("\r.");
		}
		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}


/*
*********************************************************************************************************
*                                          Switches_Task()
*
* Description : Tarea que hace lectura de los switches cada 500 ms
*
* Argument(s) : p_arg       Argument passed to 'Switches_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Switches_Task (void *p_arg)
{
	while (DEF_TRUE){
		//Utilizo las lecturas negadas ya que estoy haciendo Pull-Up
		switch1=HAL_GPIO_ReadPin(Sw1_GPIO_Port, Sw1_Pin); // Start-Stop
		switch2=HAL_GPIO_ReadPin(Sw2_GPIO_Port, Sw2_Pin); // Reset
		switch3=HAL_GPIO_ReadPin(Sw3_GPIO_Port, Sw3_Pin); // Ascendente
		switch4=HAL_GPIO_ReadPin(Sw4_GPIO_Port, Sw4_Pin); // Descendente

		if(!switch1){
			start=!start;
			UsbPrintf("\nStart: %d\n",start);
		}else if(!switch2){
			if(!asc_desc){
				UsbPrintf("\rContador reiniciado para modo ascendente     \n");
				timer_min=0;
				timer_sec=0;
				timer_ms=0;
			}else{
				UsbPrintf("\n\rContador reiniciado para modo descendente  \n");
				timer_min=12;
				timer_sec=0;
				timer_ms=0;
			}
		}else if(!switch3){
			if(asc_desc){
				UsbPrintf("\rModo cambiado a Ascendente                   \n");
				asc_desc=0;
			}
		}else if(!switch4){
			if(!asc_desc){
				UsbPrintf("\rModo cambiado a Descendente                  \n");
				asc_desc=1;
			}
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}

/*
*********************************************************************************************************
*                                          Digital_Clock_Task()
*
* Description : Tarea que actualiza el contador del reloj.
*
* Argument(s) : p_arg       Argument passed to 'Digital_Clock_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Digital_Clock_Task (void *p_arg)
{
	while (DEF_TRUE){
		if(start){
			if(!asc_desc){ //Si estoy ascendiendo
				if(timer_ms<9){
					timer_ms++;
				}else{
					timer_ms=0;
					if(timer_sec<59){
						timer_sec++;
					}else{
						timer_sec=0;
						timer_min++;
					}
				}
			}else{			//Si estoy descendiendo
				if(timer_ms>0){
					timer_ms--;
				}else{
					timer_ms=9;
					if(timer_sec>0){
						timer_sec--;
					}else{
						if(timer_min<=0){
							UsbPrintf("\n");
							start=0;		// Si el contador llega a 0, se detiene.
						}else{
							timer_sec=59;
							--timer_min;
						}
					}
				}
			}
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

/*
*********************************************************************************************************
*                                          Show_Clock_Task()
*
* Description : Tarea que actualiza el contador del reloj.
*
* Argument(s) : p_arg       Argument passed to 'Show_Clock_Task()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Show_Clock_Task (void *p_arg)
{
	while (DEF_TRUE){
		if(start){
			if(!asc_desc)
				UsbPrintf("\rTipo: Ascendente - %02d:%02d:%01d", timer_min, timer_sec, timer_ms);
			else
				UsbPrintf("\rTipo: Descendente - %02d:%02d:%01d", timer_min, timer_sec, timer_ms);
		}

		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}


/*
**************************************************************************************************************************
*                                               UsbPrintfMsg()
*
* Description :
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/
void UsbPrintf (CPU_CHAR  *p_fmt, ...)
{
    CPU_CHAR    str[80u + 1u];
    CPU_SIZE_T  len;
    va_list     vArgs;

    va_start(vArgs, p_fmt);

    vsprintf((char       *)str,
             (char const *)p_fmt,
                           vArgs);

    va_end(vArgs);

    len = Str_Len(str);

    CDC_Transmit_FS((uint8_t *)str, len);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
