/*
 * secuencia_2.h
 *
 *  Created on: Apr 8, 2022
 *      Author: Agus
 */

#ifndef INC_SECUENCIA_2_H_
#define INC_SECUENCIA_2_H_

#define N 4

void sec2_sign_1();
void sec2_sign_2();
void sec2_sign_3();
void secuencia_2();


#endif /* INC_SECUENCIA_2_H_ */
