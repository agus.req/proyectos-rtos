/*
 * secuencia_3.h
 *
 *  Created on: Apr 8, 2022
 *      Author: Agus
 */

#ifndef INC_SECUENCIA_3_H_
#define INC_SECUENCIA_3_H_

#define N 4
#define M 6
#define L 7

void sec3_sign_1();
void sec3_sign_2();
void sec3_sign_3();
void secuencia_3();

#endif /* INC_SECUENCIA_3_H_ */
