/*
 * secuencia_3.c
 *
 *  Created on: Apr 8, 2022
 *      Author: Agus
 */

#include "main.h"
#include "secuencia_3.h"

extern int count1;
extern int count2;
extern int count3;

void sec3_sign_1(){
	static const int s3_patron_1[N] = {1,1,0,0};

	if(count1<=N-1){
		HAL_GPIO_WritePin(LED_b0_GPIO_Port, LED_b0_Pin, s3_patron_1[count1]);
	}

	if (count1>=N-1){
		count1=0;
	}else{
		count1++;
	}
}

void sec3_sign_2(){
	static const int s3_patron_2[M] = {1,1,1,0,0,0};

	if(count2<=M-1){
		HAL_GPIO_WritePin(LED_b1_GPIO_Port, LED_b1_Pin, s3_patron_2[count2]);
	}

	if (count2>=M-1){
		count2=0;
	}else{
		count2++;
	}
}

void sec3_sign_3(){
	static const int s3_patron_3[L] = {1,1,1,1,0,0,0};

	if(count3<=L-1){
		HAL_GPIO_WritePin(LED_b2_GPIO_Port, LED_b2_Pin, s3_patron_3[count3]);
	}

	if (count3>=L-1){
		count3=0;
	}else{
		count3++;
	}
}


void secuencia_3(){
	//La señal ya entra con 50ms o (5 * Delay) por lo tanto aqui no hacemos el contador de 2
	sec3_sign_1();
	sec3_sign_2();
	sec3_sign_3();
}
