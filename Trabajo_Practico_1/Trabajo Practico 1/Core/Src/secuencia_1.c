/*
 * secuencia_1.c
 *
 *  Created on: Apr 8, 2022
 *      Author: Agus
 */

#include "main.h"
#include "secuencia_1.h"

extern int count2;

void sec1_sign_1(){
	HAL_GPIO_TogglePin(LED_b0_GPIO_Port, LED_b0_Pin);
}

void sec1_sign_2(){

	if (count2>=1){
		HAL_GPIO_TogglePin(LED_b1_GPIO_Port, LED_b1_Pin);
		count2=0;
	}else{
		count2++;
	}
}


void secuencia_1(){
	//Siempre entro con 50ms o (5 * Delay). (Elegi ese valor ya que es el multiplo comun de todos los tiempos de señal)
	static int count = 0;

	if (count>=1){
		//Transformo los pulsos de 50ms en 100ms y ejecuto los juegos de luces

		sec1_sign_1();
		sec1_sign_2();

		count = 0;
	}else{
		count++;
	}


}
