/*
 * menu.c
 *
 *  Created on: Apr 8, 2022
 *      Author: Agus
 */

#include "menu.h"
//#include "stdio.h"
#include "main.h"

extern int count1;
extern int count2;
extern int count3;

void res_sec(){
	HAL_GPIO_WritePin(LED_b0_GPIO_Port, LED_b0_Pin, RESET); //Reseteo los 3 pines de frecuencia
	HAL_GPIO_WritePin(LED_b1_GPIO_Port, LED_b1_Pin, RESET);
	HAL_GPIO_WritePin(LED_b2_GPIO_Port, LED_b2_Pin, RESET);
	count1=0;
	count2=0;
	count3=0;
}

extern int secu;
uint8_t button1 = 0;
uint8_t button2 = 0;
uint8_t button3 = 0;

void menu(){
	if (!HAL_GPIO_ReadPin(Sw_1_GPIO_Port, Sw_1_Pin)){
		res_sec();
		secu = 1;
		HAL_GPIO_WritePin(LED_b0_GPIO_Port, LED_b0_Pin, SET); //Hago esto asi ya empieza el led B0 en alto y no en bajo
		HAL_GPIO_WritePin(LED_b1_GPIO_Port, LED_b1_Pin, SET); //Hago esto asi ya empieza el led B1 en alto y no en bajo
		HAL_GPIO_WritePin(LED_st1_GPIO_Port, LED_st1_Pin, SET);
		HAL_GPIO_WritePin(LED_st2_GPIO_Port, LED_st2_Pin, RESET);
		HAL_GPIO_WritePin(LED_st3_GPIO_Port, LED_st3_Pin, RESET);
	}else if (!HAL_GPIO_ReadPin(Sw_2_GPIO_Port, Sw_2_Pin)){
		res_sec();
		secu = 2;
		HAL_GPIO_WritePin(LED_st1_GPIO_Port, LED_st1_Pin, SET);
		HAL_GPIO_WritePin(LED_st2_GPIO_Port, LED_st2_Pin, SET);
		HAL_GPIO_WritePin(LED_st3_GPIO_Port, LED_st3_Pin, RESET);
	}else if (!HAL_GPIO_ReadPin(Sw_3_GPIO_Port, Sw_3_Pin)){
		res_sec();
		secu = 3;
		HAL_GPIO_WritePin(LED_st1_GPIO_Port, LED_st1_Pin, SET);
		HAL_GPIO_WritePin(LED_st2_GPIO_Port, LED_st2_Pin, SET);
		HAL_GPIO_WritePin(LED_st3_GPIO_Port, LED_st3_Pin, SET);
	}

}
